package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const (
	CAT_FACTS_API   = "https://catfact.ninja"
	BREEDS_ENDPOINT = "/breeds"
	FACTS_ENDPOINT  = "/facts"
)
type catFact struct{
	Data [] struct{
		Fact string `json:"fact"`
	} `json:"data"`
}
func GetCatFacts() ([]string, error) {
	factsUrl := CAT_FACTS_API + FACTS_ENDPOINT
	resp, err := http.Get(factsUrl)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
   var cf catFact
	err = json.Unmarshal(body, &cf)
	if err != nil {
		return nil, err
	}
	var facts [] string
	for _, fact := range cf.Data{
		facts = append(facts, fact.Fact)
	}
	return facts, nil
}


func main() {
	facts, err := GetCatFacts()
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range facts{
		fmt.Println(f)
	}
}
